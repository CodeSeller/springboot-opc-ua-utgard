package opc.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Classname Phase2Controller
 * @Description TODO
 * @Date 2021/9/9 10:59
 * @Created by kkk
 */
@RequestMapping("/aaa")
@Controller
public class Phase2Controller {
    private static final Logger logger = LoggerFactory.getLogger(Phase2Controller.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:mm:dd HH:mm:ss");

    @Autowired
    private Phase2Service phase2Service;

    /**
     * 获取二期发电数据（三、四号机组），保存到数据库中(x分钟调度一次)
     */
    @GetMapping("/list")
    public void searchGasData() {
        logger.info("####获取二期发电数据（三、四号机组） - 定时任务执行时间：" + dateFormat.format(new Date()));
        phase2Service.searchPhase2ElectricData();
    }
}
