package opc.client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.identity.AnonymousProvider;
import org.eclipse.milo.opcua.sdk.client.api.identity.IdentityProvider;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;

import java.util.concurrent.CompletableFuture;

/**
 * @Classname ClientExample
 * @Description TODO
 * @Date 2021/9/9 17:10
 * @Created by kkk
 */
public interface ClientExample {
    default String getEndpointUrl() {
        return "opc.tcp://127.0.0.1:49320";
    }

    default SecurityPolicy getSecurityPolicy() {
        return SecurityPolicy.None;
    }

    default IdentityProvider getIdentityProvider() {
        return new AnonymousProvider();
    }

    void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception;
}
