package opc.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Classname Phase2ServiceImpl
 * @Description TODO
 * @Date 2021/9/9 10:58
 * @Created by kkk
 */
@Service("phase2Service")
public class Phase2ServiceImpl implements Phase2Service {
    @Autowired
    private OpcUAClientService opcUAClientService;

    /**
     * 获取二期发电数据（三、四号机组），保存到数据库中
     */
    @Override
    public void searchPhase2ElectricData() {
        new OpcUAClientRunner(opcUAClientService).run();
    }

}
