##springboot-opc-ua-utgard 开发记录

#### 介绍
通过opcUA方式接入工业设备数据、服务端采用KEPServerEX 6 Configuration工具，工具请自行下载
opcUA优势 无需配置DCOM、相对DA和Jeasy opc更加简单，我最开始也是DA方式接入，后来都更改为UA方式了。

#### DA方式请参考
https://gitee.com/hellokkk/springboot-opc-da-utgard/blob/master/README.en.md

#### 工具
下载地址：https://www.kepware.com/zh-cn/、

KEPServer中文官网：https://www.kepware.com/zh-cn/products/kepserverex/

KEPServer中文官方文档（经常更新，其实就内置的帮助文档内容）：https://www.kepware.com/getattachment/af76712f-a703-443d-a592-b9621382d224/kepserverex-manual_zh-cn.pdf

学习及参考链接：https://www.cnblogs.com/ioufev/p/9366877.html

#### 遇到的坑

1.  选择开启匿名方式访问（也可不开启，如不开启请见下面设置账户密码，并在代码中配置）
   
    ![img_4.png](img_4.png)
2.请注意设置账号密码
    
    ![img.png](img.png)
2.  注意事项  （是否开启安全策略和网络适配器选择 我自己在本机，开启的安全策略）
    ![img_2.png](img_2.png)
3.  导入证书（匿名方式好像不需要证书。。。）。（证书在resource目录下example-client.pfx）
    ![img_3.png](img_3.png)
    
    

#### 使用说明
1.配置自己的账号和密码
![img_1.png](img_1.png)

2.直接运行RunDemo文件
![img_5.png](img_5.png)

#### 友情链接
1、安装 KEPServerEX 6
https://blog.csdn.net/han_better/article/details/81624924

2、使用java的milo框架访问OPCUA服务的方法 帮助参考
https://blog.csdn.net/yhj_911/article/details/107710566?utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~aggregatepage~first_rank_ecpm_v1~rank_aggregation-1-107710566.pc_agg_rank_aggregation&utm_term=milo%E5%BC%80%E5%8F%91+opc&spm=1000.2123.3001.4430

3、JAVA使用OPC UA 方式与设备通信（milo）
https://blog.csdn.net/u010695169/article/details/115759926?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1.pc_relevant_default&utm_relevant_index=2
   
4、Java(Springboot)订阅opc ua 批量订阅
https://haochangqi.blog.csdn.net/article/details/116202941?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2.pc_relevant_default&utm_relevant_index=4
